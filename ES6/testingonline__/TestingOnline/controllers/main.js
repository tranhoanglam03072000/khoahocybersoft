import { data_question } from "../data/questions.js";
import { checkQuestion, renderQuestion } from "./questionController.js";

let currentQuestionIndex = 0;
//Lần đầu khi userClick
renderQuestion(
  data_question[currentQuestionIndex],
  currentQuestionIndex,
  data_question.length
);
let handleNextQuestion = () => {
  checkQuestion(data_question[currentQuestionIndex]);
  currentQuestionIndex++;
  let currentQuestion = data_question[currentQuestionIndex];
  if (currentQuestionIndex >= data_question.length) {
    return;
  }
  renderQuestion(currentQuestion, currentQuestionIndex, data_question.length);
};
window.handleNextQuestion = handleNextQuestion;
