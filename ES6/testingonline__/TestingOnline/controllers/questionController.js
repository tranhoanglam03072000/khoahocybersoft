let renderRadioButton = (data) => {
  return /*html*/ `<div class="form-check col-12">
    <label class="form-check-label">
      <input
        type="radio"
        class="form-check-input"
        name="singleChoice"
        id=""
        value=${data.exact}
      />
      ${data.content}
    </label>
  </div>`;
};
let renderSingleChoice = (question) => {
  let contentHTML = /*html*/ `
  <p>${question.content}</p>`;
  question.answers.forEach((item) => {
    contentHTML += renderRadioButton(item);
  });
  document.querySelector("#quiz_content").innerHTML = contentHTML;
};
let renderFillInBlank = (question) => {
  let contentHTML = /*html*/ `
    <p class="col-12" >${question.content}</p>
    <br>
    <div class="form-group">
  <input data-no-answer='${question.answers[0].content}' type="text" class="form-control" id="fillToInput" />
</div>`;

  document.querySelector("#quiz_content").innerHTML = contentHTML;
};

let renderQuestion = (question, currentQuestionIndex, total) => {
  //Show stt hiện tại
  document.getElementById("current_step").innerHTML = `${
    currentQuestionIndex + 1
  }/${total}`;
  if (question.questionType == 1) {
    // gọi hàm render sigle choice
    renderSingleChoice(question);
  } else {
    //gọi hàm render fill in blank
    renderFillInBlank(question);
  }
};
let checkSingleChoice = () => {
  if (!document.querySelector('input[name="singleChoice"]:checked')) {
    return false;
  }
  let useAnswer = document.querySelector(
    'input[name="singleChoice"]:checked'
  ).value;
  if (useAnswer == "true") {
    return true;
  } else {
    return false;
  }
};
let checkFillInBlank = () => {
  let inputEl = document.getElementById("fillToInput");
  return inputEl.value == inputEl.dataset.noAnswer;
};
let checkQuestion = (question) => {
  if (question.questionType == 1) {
    //gọi hàm check singleChoice
    checkSingleChoice();
  } else {
    checkFillInBlank();
    //gọi hàm check fill in blank
  }
};
export { renderQuestion, checkQuestion };
