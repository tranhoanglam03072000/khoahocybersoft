function Dog(name, age, gender) {
  this.name = name;
  this.age = age;
  this.gender = gender;
}
let bull = new Dog("bun", 2, "male");
console.log("bull: ", bull);

class Cat {
  name;
  age;
  gender;
  constructor(Name, Age, Gender) {
    this.name = Name;
    this.age = Age;
    this.gender = Gender;
  }
}
let meo = new Cat("bun", 2, "male");
console.log("meo: ", meo);
