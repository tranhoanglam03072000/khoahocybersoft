// Tinh kế thừa

class smartPhone {
  constructor(name, color, weight) {
    this.color = color;
    this.weight = weight;
    this.name = name;
  }
  recording() {
    console.log("Recoreding..");
    return 3;
  }
}

class iPhone extends smartPhone {
  constructor(name, color, weight, system) {
    super(name, color, weight, system);
    this.system = system;
  }
  useLightning() {
    console.log("use Lightning");
  }
  recording() {
    let a = super.recording();
    console.log("a: ", a);
    console.log("first");
  }
}
class samSung extends smartPhone {}

let iphone = new iPhone("lam", "black", "20px", "IOS");
iphone.recording();
iphone.useLightning();
console.log("iphone: ", iphone);
// Tính đóng gói
class sony extends smartPhone {
  #height = 100;
  constructor(name, color, weight, system) {
    super(name, color, weight);
    this.system = system;
  }
  showHeight() {
    console.log(this.#height);
  }
  Number() {
    console.log(123);
  }
  static show() {}
}

let Sony = new sony("sony", "black", 600, "adr");
console.log("Sony: ", Sony);
Sony.showHeight();
sony.show();
// Trừu tượng
class Animal {
  constructor(name, color) {
    this.name = name;
    this.color = color;
  }
  findFood() {}
  eat() {
    const eat1 = this.findFood();
    console.log("eating" + eat1 + "....");
  }
}

class cat extends Animal {
  findFood() {
    console.log("mèo méo meo mèo meo");
    return "méo mèo";
  }
}

class tiger extends Animal {
  findFood() {
    console.log("Đi săn mèo");
    return "mèo";
  }
}

const meo = new cat("mèo", "7 màu");
meo.eat();
const ho = new tiger("mèo", "7 màusav");
console.log("ho: ", ho);
// Tính đa hình
class Component {
  constructor() {}
  render() {
    throw new Error("Bắt buộc có hàm render");
  }
}

class Header extends Component {
  render() {
    return `<header>Header</header>`;
  }
}
const header = new Header();
header.render();
console.log("header.render(): ", header.render());
