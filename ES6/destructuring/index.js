let usename = "alcie";
let age = 2;
let sv = {
  usename,
  age,
};
console.log({ usename, age });

let user = {
  account: "Alice",
  address: {
    Number: 111,
    district: "Thu Duc",
    street: "PVD",
  },
};
let { account, address } = user;
console.log("account: ", account);

let color = ["red", "green"];
let [dowtrend, xanh] = color;
console.log("dowtrend: ", dowtrend);
