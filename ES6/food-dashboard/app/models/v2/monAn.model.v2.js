class MonAnV2 {
  constructor(id, ten, loai, gia, khuyenMai, tinhTrang, hinhAnh, moTa) {
    this.ten = ten;
    this.loai = loai;
    this.gia = gia;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhAnh = hinhAnh;
    this.moTa = moTa;
    this.id = id;
  }
}

export { MonAnV2 };
