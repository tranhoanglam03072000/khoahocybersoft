const LINKURRL = `https://62f8b754e0564480352bf3de.mockapi.io`;
import { MonAnV2 } from "../../models/v2/monAn.model.v2.js";
import {
  layThongTinTuForm,
  renderDanhSach,
  showThongTinLenForm,
  timIdMonAn,
} from "./controller.v2.js";

let dsma = [];
let turnOnLoading = () => {
  document.getElementById("loadingFood").style.display = "flex";
};
let turnOffLoading = () => {
  document.getElementById("loadingFood").style.display = "none";
};
let renDerTableService = () => {
  turnOnLoading();
  axios({
    url: `${LINKURRL}/food`,
    method: "GET",
  })
    .then((res) => {
      dsma = res.data;
      renderDanhSach(res.data);
      turnOffLoading();
    })
    .catch((err) => {
      console.log(err);
    });
};

renDerTableService();

document.getElementById("btnThemMon").onclick = () => {
  turnOnLoading();
  let data = layThongTinTuForm();
  console.log("data: ", data);
  let monAn = new MonAnV2(
    data.foodID,
    data.tenMon,
    data.loai,
    data.giaMon,
    data.khuyenMai,
    data.tinhTrang,
    data.hinhMon,
    data.moTa
  );
  console.log("monAn: ", monAn);
  axios({
    url: `${LINKURRL}/food`,
    method: "POST",
    data: monAn,
  })
    .then((res) => {
      renDerTableService();
      turnOffLoading();
    })
    .catch((err) => {
      console.log(err);
    });
};

let suaMonAn = (id) => {
  let viTriMonAn = timIdMonAn(dsma, id);
  showThongTinLenForm(dsma[viTriMonAn]);
};
window.suaMonAn = suaMonAn;
let xoaMonAn = (id) => {
  turnOnLoading();
  axios({
    url: `${LINKURRL}/food/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      renDerTableService();
      turnOffLoading();
    })
    .catch();
};
window.xoaMonAn = xoaMonAn;
/* 
truthy
: [],{}


falsy
undefined,null, false, NaN,0,""

*/
