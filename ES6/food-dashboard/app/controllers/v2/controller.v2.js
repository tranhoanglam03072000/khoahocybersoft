let domSelec = (id) => {
  return document.querySelector(id);
};
let renderDanhSach = (list) => {
  let contentHTML = ``;
  list.forEach((item) => {
    let contentTr = `
    <tr>
    <td>${item.id}</td>
    <td>${item.ten}</td>
    <td>${item.loai ? "Mặn" : "Chay"}</td>
    <td>${item.gia.toLocaleString()}</td>
    <td>${item.khuyenMai}</td>
    <td>000</td>
    <td>${item.tinhTrang ? "còn" : "hết"}</td>
    <td>
    <button class="btn btn-success" onclick="suaMonAn('${
      item.id
    }')">Sửa</button>
    <button class="btn btn-success" onclick="xoaMonAn('${
      item.id
    }')">Xóa</button>
    </td>
    </tr>
    `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
let layThongTinTuForm = () => {
  let foodID = domSelec("#foodID").value;
  let tenMon = domSelec("#tenMon").value;
  let loai = domSelec("#loai").value !== "loai1" ? true : false;
  let giaMon = domSelec("#giaMon").value;
  let khuyenMai = domSelec("#khuyenMai").value;
  let tinhTrang = domSelec("#tinhTrang").value !== "0" ? true : false;
  let hinhMon = domSelec("#hinhMon").value;
  let moTa = domSelec("#moTa").value;
  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
};

let timIdMonAn = (arr, id) => {
  let index = arr.findIndex((vitri) => vitri.id == id);
  return index;
};
let showThongTinLenForm = (arr) => {
  let { gia, hinhAnh, id, khuyenMai, loai, moTa, ten, tinhTrang } = arr;
  console.log(arr);
  domSelec("#foodID").value = id;
  domSelec("#tenMon").value = ten;
  domSelec("#loai").value = loai == false ? "loai1" : "loai2";
  domSelec("#giaMon").value = gia;
  domSelec("#khuyenMai").value = khuyenMai;
  domSelec("#tinhTrang").value = tinhTrang == true ? "1" : "0";
  domSelec("#hinhMon").value = hinhAnh;
  domSelec("#moTa").value = moTa;
};
export { layThongTinTuForm, renderDanhSach, timIdMonAn, showThongTinLenForm };
