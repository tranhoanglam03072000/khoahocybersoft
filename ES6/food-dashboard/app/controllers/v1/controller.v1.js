let domSelec = (id) => {
  return document.querySelector(id);
};
let layThongTinTuForm = () => {
  let foodID = domSelec("#foodID").value;
  let tenMon = domSelec("#tenMon").value;
  let loai = domSelec("#loai").value;
  let giaMon = domSelec("#giaMon").value;
  let khuyenMai = domSelec("#khuyenMai").value;
  let tinhTrang = domSelec("#tinhTrang").value;
  let hinhMon = domSelec("#hinhMon").value;
  let moTa = domSelec("#moTa").value;
  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
};
let showThongTinLenForm = (monAn) => {
  console.log(monAn);
  let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    monAn;
  domSelec("#spMa").innerText = foodID;
  domSelec("#spTenMon").innerText = tenMon;
  domSelec("#spLoaiMon").innerText = loai == "loai1" ? "Chay" : "Mặn";
  domSelec("#spGia").innerText = giaMon;
  domSelec("#spKM").innerText = `${khuyenMai} %`;
  domSelec("#spTT").innerText = tinhTrang;
  domSelec("#pMoTa").innerText = moTa;
  domSelec("#spGiaKM").innerText = monAn.tinhGiaKm();
  domSelec("#imgMonAn").src = hinhMon;
};
export { layThongTinTuForm, domSelec, showThongTinLenForm };
