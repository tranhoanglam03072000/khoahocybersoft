import { MonAnV1 } from "../../models/v1/monAn.model.v1.js";
import { layThongTinTuForm, showThongTinLenForm } from "./controller.v1.js";
function themMon() {
  let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } =
    layThongTinTuForm();
  let monAn = new MonAnV1(
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa
  );
  showThongTinLenForm(monAn);
}
window.themMon = themMon;
