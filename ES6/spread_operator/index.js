let sv1 = {
  toan: 10,
  van: 10,
};

let sv2 = { ...sv1, toan: 1 }; //shallow clone
sv2.toan = 1;
console.log("sv2.toan: ", sv2.toan);

let dssv = [sv1, sv2];
let sv3 = {
  toan: 3,
  van: 3,
};
let newDssv = [...dssv, sv3];
console.log("newDssv: ", newDssv);
