// scope ~ pham vu hoat dong

// let ~ function scope
var isLogin = true;
function checkLogin1() {
  if (isLogin) {
    var loginTime = "today";
  }
  console.log(loginTime); // oke
}
// console.log(loginTime); // error
// checkLogin1();

// let ~ block scope

let isLogin2 = true;
function checkLogin2() {
  if (isLogin2) {
    let loginTime = "today";
  }
  console.log(loginTime); // err
}
checkLogin2();
