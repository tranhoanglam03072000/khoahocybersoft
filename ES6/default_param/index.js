let tinhDTB = (toan, ly, hoa = 0) => {
  return (toan + ly + hoa) / 3;
};

let tinhDtb1 = tinhDTB(2, 2, 2);
console.log("tinhDtb1: ", tinhDtb1);
let tinhDtb2 = tinhDTB(2, 2);
console.log("tinhDtb2: ", tinhDtb2);

let renderThongBao = (isSuccess, onSuccess = () => {}, onFail = () => {}) => {
  if (isSuccess) {
    console.log("thanh cong");
    onSuccess();
  } else {
    onFail();
  }
};

let renderSuccessLogin = () => {
  console.log("dang nhap thanh cong");
};

let renderSuccessUpdate = () => {
  console.log("cap nhat thanh cong");
};
let renderFail = () => {
  console.log("dang nhap that bai");
};
renderThongBao(false);
