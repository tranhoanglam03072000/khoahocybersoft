console.log(10);
console.log(20);

//

setTimeout(() => {
  console.log(30);
}, 1000);

console.log(40);

// có 3 cách xử lý bất đồng bộ
//1.  Sử dụng callback (ES5) => sinh ra vấn đề là callback hell
//2. Sử dụng promise(ES6) => vấn đề promise chain
//3. sử dụng async await (ES7) => bản chất vẫn là promise
const getDaTa = (data) => {
  console.log(data);
};

const callAPI = (getDaTa) => {
  let data;
  setTimeout(() => {
    data = "100";
    getDaTa(data);
  }, 1000);
  //   return data;
};
const data = callAPI(getDaTa);
console.log("data: ", data);

// promise
// Có 3 trạng thái
//1. Pending: chờ reject hoặc resolve
// 2 . fulfilled : Thành công
// 3. Rejected: Thất bại

const promise = new Promise((resolve, rejecttttt) => {
  //   resolve("thành công");
  rejecttttt("Thất bại");
});

promise
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  })
  .finally(() => {});
console.log(promise);

// Ví dụ : tính diện tích hình thang
const tinhTong2Day = (a, b) => {
  const promise = new Promise((resolve, rejecttttt) => {
    setTimeout(() => {
      //   resolve(a + b);
      rejecttttt("looix");
    }, 1000);
  });
  return promise;
};

const tinhDienTich = (sum, h) => {
  const promise = new Promise((resolve, rejecttttt) => {
    setTimeout(() => {
      resolve((sum * h) / 2);
    }, 1000);
  });
  return promise;
};

const sum = tinhTong2Day(3, 4);

sum
  .then((res) => {
    return tinhDienTich(res, 5);
  })
  .then((dienTich) => {
    console.log(dienTich);
  })
  .catch((err) => {});

const tihDienTich1 = async () => {
  try {
    const tong2Day = await tinhTong2Day(3, 4);
    const dienTich = await tinhDienTich(tong2Day, 5);
    console.log(dienTich);
  } catch (err) {
    console.log(err);
  }
};

const value = tihDienTich1();
