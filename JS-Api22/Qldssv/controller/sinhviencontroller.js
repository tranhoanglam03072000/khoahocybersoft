function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var hinhAnh = document.getElementById("txtImg").value;
  return {
    ma: ma,
    ten: ten,
    email: email,
    hinhAnh: hinhAnh,
  };
}

function hienThiThongTinLenForm(dssv) {
  document.getElementById("txtMaSV").value = dssv.ma;
  document.getElementById("txtTenSV").value = dssv.ten;
  document.getElementById("txtEmail").value = dssv.email;
  document.getElementById("txtImg").value = dssv.hinhAnh;
}
