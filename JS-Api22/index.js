var dssv = [];
var BASE_URL = "https://62f8b754e0564480352bf3de.mockapi.io";
var turnonLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var turnofLoading = function () {
  document.getElementById("loading").style.display = "none";
};
var rendertable = function (list) {
  var contentHTML = ``;
  list.forEach(function (item) {
    var trContent = `<tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td><img src=${item.hinhAnh} style="width: 80px;" alt="" /></td>
    <td>
    <Button class="btn btn-success" onclick="xoaSinhVien('${item.ma}')">Xoa</Button>
    <Button class="btn btn-success" onclick="suaSinhVien('${item.ma}')">Sua</Button>
    </td>
    </tr>
    `;
    contentHTML += trContent;
  });
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
};
var renderDssvService = function () {
  turnonLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      turnofLoading();
      dssv = res.data;
      rendertable(dssv);
    })
    .catch(function (err) {
      console.log(err);
    });
};
renderDssvService();
function xoaSinhVien(id) {
  turnonLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      renderDssvService();
      turnofLoading();
    })
    .catch(function (err) {
      turnofLoading();
      console.log(err);
    });
}

function themSv() {
  turnonLoading();
  var dataForm = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      turnofLoading();
      renderDssvService();
    })
    .catch(function (err) {
      turnofLoading();
    });
}

function suaSinhVien(id) {
  turnonLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnofLoading();
      hienThiThongTinLenForm(res.data);
      document.getElementById("txtMaSV").disabled = true;
    })
    .catch(function (err) {
      turnofLoading();
      console.log(err);
    });
}

// Cập nhật dùng PUT
function capNhatSv() {
  // turnonLoading();
  var dataForm = layThongTinTuForm();
  // console.log("dataForm : ", dataForm);
  var id = dataForm.ma;
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      turnofLoading();
      renderDssvService();
      document.getElementById("formQLSV").reset();
      document.getElementById("txtMaSV").disabled = false;
    })
    .catch(function (err) {
      turnofLoading();
      console.log(err);
    });
}
